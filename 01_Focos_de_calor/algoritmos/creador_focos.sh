#!/bin/bash

#### 
# Script para descarga y preprocesamiento automático de focos de calor de los sensores VIIRS y MODIS
### Leandro Cara. 
#
#  leandrocara@hotmail.com 
#    5 de Octubre 2018
#
#
#################################### ruta donde se encuentran los sahpes diarios
ruta_shapes="./points"
###################################

SCRIPT=`realpath $0`
dir=`dirname $SCRIPT`


logger1 () {
clear
x1=""
x2="##########################################################################"
x3="    --------------------------------------------------     "
echo $x1;echo $x1; echo $x2;echo $x2;echo $x1; echo $1 ; echo $x1; echo $x2;echo $x2;echo $x3; echo $x1;echo $x1; date 
}

logger2 () {
x1=""
x2="##########################################################################"

echo $x1;echo $x1; echo $x2;echo $x1; echo $1 ; echo $x1;echo $x2; echo $x1;echo $x1; echo $x3
}	


page="https://nrt3.modaps.eosdis.nasa.gov"
servidor="https://nrt3.modaps.eosdis.nasa.gov/api/v2/content/archives/FIRMS/"
server=( "c6/South_America/" "c6/Central_America/" "viirs/South_America/" "viirs/Central_America/") 
apoyo=( "./.apoyo/modis_sur.txt" "./.apoyo/modis_centro.txt" "./.apoyo/viirs_sur.txt" "./.apoyo/viirs_centro.txt")
relleno="XXXX,XXXX"
app_key="A22AA23E-C8CB-11E9-8642-6D5C33EB2967"


logger1 "Creador de focos de calor"

### me paso al directorio de trabajo
cd $dir	


### compruebo si existen 
rm -f *.txt >/dev/null 
rm -f index.* >/dev/null 

echo $ruta_shapes > ruta.txt


#para cada uno de los productos que son 4 pero se juntan de a dos!!!

for i in `seq 0 3`

do
# remuevo el index
rm -f index.* >/dev/null 
logger2 "Iniciando ciclo de descarga  de focos de calor"

wget "$servidor/${server[$i]}" --header "Authorization: Bearer $app_key" 

if test -f  index.html ; then echo "";   echo "lista de focos de calor descargada" ; echo "" ; else  echo "Ha ocurrido un problema con la descarga del archivo debido a problemas en el servidor, acceso no posible" ; exit 0 ; fi


Rscript  ./fecha.R # de fecha sale el nombre del archivo a descargar y la fecha de la última actualización desde el servidor de nasa!
 descarga=`cat fecha.csv`
 pr2=${descarga%,*}
 descarga=${descarga#*,}


  wget "$page$descarga" --header "Authorization: Bearer $app_key" 
 
if test -f  *$pr2.* ; then echo "";   echo "Archivo de focos de calor descargado" ; echo *$pr2.*; echo "" ; else  echo "Ha ocurrido un problema con la descarga del archivo debido a problemas en el servidor" ; exit 0 ; fi

echo "";echo ""
Rscript ./txt2shp.R
echo "";echo ""

### no se que buscaba con esto pero no está bien 

echo $apoyo
echo "";echo ""

 rm fecha.csv
 Rscript ./binder.R

# fi

done
rm ruta.txt

############################################################################################



