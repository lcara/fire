rm(list = ls())
cat("\n#>>>>> Transformando la lista de puntos descargada avector espacial <<<<<#\n\n\n")
library(raster)

r.shapes<- as.character(read.csv("ruta.txt",header =F)[,1])
corte.x <- function(x,y=NULL,z=NULL){ while(grepl("/",x[1])){
  x<- substr(x,grep("/",x[1])+1,nchar(x))}
  if(!is.null(y) & !is.null(z)){
  x<- substr(x,y,z)}
  return(x)}

fecha <- read.csv("fecha.csv",header = F,as.is = T)[2]
fecha<- corte.x(fecha)
if(length(list.files(pattern = fecha))==0){
  cat( "Ha ocurrido un problema con la descarga del archivo, 
       esto puede ser debido a problemas en el servidor\n" )
  quit()
}
x<- read.csv(fecha,as.is = T)
#names(x)
x <- x[,c(1,2,1,2,6:9,12)]
x[,1] <- round(x[,1],4)
x[,2] <- round(x[,2],4)
reg<- c("Central","South")

x$region <-reg[c(grepl(reg[1],x = fecha),grepl(reg[2],x = fecha))] 
## los satélites tienen las denominaciones A T y N?

if(any(unique(x$satellite)=="T"|unique(x$satellite)=="A")){
ind1<- which(x$confidence>=65) 
ind2<- which(x$confidence<65 & x$confidence>=35)
ind3<- which(x$confidence<35) 
x$confidence[ind1] <- "high"
x$confidence[ind2] <- "nominal"
x$confidence[ind3] <- "low"

x$satellite[which(x$satellite=="A")] <- "MODIS Aqua"

x$satellite[which(x$satellite=="T")] <- "MODIS Terra"

}else if(unique(x$satellite)=="N"){
  x$satellite <- " VIIRS"
}
coordinates(x)=~longitude.1+latitude.1
proj4string(x)<- CRS("+proj=longlat +datum=WGS84")
cat("\n\n\n")
cat("Nuevo shape de puntos generado:\n")
print(x)
cat("\n\n\n")
shapefile(x,paste0(r.shapes,"/",gsub(".txt",".shp",fecha)),overwrite=T)
print(paste("Archivo a Borrar:", fecha))
unlink(fecha)
if(!file.exists(fecha)){
  print(paste(fecha,"|","eliminado para comenzar un nuevo proceso"))
}else{
  print(paste("Se produjo un error al borrar el archivo","|",fecha))
  }
