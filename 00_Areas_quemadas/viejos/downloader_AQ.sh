#!/bin/bash
###################################################################################################################
# descarga  MCD64A1 de ftp 
# leandrocara@hotmail.com
## 
 
##############
## escribe la ruta de acceso al script.
path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#### cambiar a conveninencia!
temp="$path/temp";hdf="$path/HDF";aqa="$path/AQA";resample="/home/lean/MRT/bin/resample"

echo ""
echo ""
echo "#########################################################################"
echo "#########################################################################"
echo "ejecutando buscador_fecha.R"
echo "#########################################################################"
echo "#########################################################################"
echo ""
echo ""

## busca la fecha a descargar información desde el watchdog.txt
Rscript $path/busca_fecha.R


year=`cat $temp/x.txt | awk -F' ' '{print $1}'`
jd=`cat $temp/x.txt | awk -F' ' '{print $2}'`

echo $year
echo $jd
#echo "ftp://ba1.geog.umd.edu/Collection6/HDF/$year/$jd/ --user user:burnt_data > $temp/lista.txt"

### descarga una lista de imágenes para ver si hay información disponible.
curl ftp://ba1.geog.umd.edu/Collection6/HDF/$year/$jd/ --user user:burnt_data > $temp/lista.txt

echo ""
echo ""
echo "#########################################################################"
echo "#########################################################################"
echo "ejecutando resumidor_lista.R"
echo "#########################################################################"
echo "#########################################################################"
echo ""
echo ""
### busca de la lista las imágenes que necesito
Rscript $path/resumidor_lista.R

lista_res="$temp/lista_resumida.txt"


j=`wc --lines $lista_res | awk -F' ' '{print $1}'`

### borro el contenido de la carpeta hdf
w1=` ls $hdf/ | wc -l` # leo las imágenes hdf que se acaban de generar

if [ $w1 -ne 0 ]
then 
rm $hdf/*.*
fi

#############################################
### descargo las imágenes de lista resumida
for i in `seq 1 $j`;
do
echo "loop nro:"
echo $i
  x=`sed -n "${i}p" $lista_res`
  curl ftp://ba1.geog.umd.edu/Collection6/HDF/$year/$jd/$x --user user:burnt_data > $hdf/$x
done
#############################################

# leo la cantidad de img hdf que tengo descargadas x el proceso anterior
w1=` ls $hdf/*.hdf | wc -l` # leo las imágenes hdf que se acaban de generar
echo $w1

# si hay imágenes hdf dentro de la carpeta de HDF (resampleadas pero sin bit transf.)

echo ""
echo ""
echo "#########################################################################"
echo "#########################################################################"
echo "ejecutando transformador.R (bucle)"
echo "#########################################################################"
echo "#########################################################################"
echo ""
echo ""


if [ $w1 -ne 0 ]
then
  echo "trus"
  for i in $hdf/*.hdf
  do
    $resample -i $i -o ${i%.hdf}.tif -p $path/parametros.prm ### modificar ruta de 
    ### si no se quiere eliminar los temporales comentar la siguiente línea
    # queda modificar el nombre tentativamente se pueden enviar a otra carpeta!	
  done
    Rscript $path/transformador.R # 
    #rm ${i%.hdf}.Burn_Date.tif ; rm $i 
fi

#### HASTA ACÁ TODAS LAS IMÁGENES TRANSFORMADAS A TIF 8 BITS. 

mosaic=`ls $hdf/*.tif | head -1`;mosaic=${mosaic##*/};mosaic=${mosaic%"_h"*};mosaic=`echo $aqa/$mosaic".tif"`;entrada=`ls $hdf/*.tif`

python $path/gdal_merge.py -o $mosaic -n 0 -ot 'Byte' $entrada
Rscript $path/transformador2.R
mv $aqa/x.tif $mosaic

### acá quedan los tif en la carpeta de HDF y el nuevo tif en la carpeta de AQA


if [ -f $mosaic ]
then 
  mv $temp/x.txt $path/watchdog.txt
  rm $hdf/*.*
  rm $temp/*.*
else
  echo "Algo ha fallado"
fi	


