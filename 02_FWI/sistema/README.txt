Sistema de creación de índice de riesgo de incendios FWI (Fire Weather Index)

Leandro Cara 
leandrocara@hotmail.com
19 de Diciembre de 2018. 


EL FWI es dependiente de variables climáticas, teniendo en cuenta  temperatura, precipitación, humedad relativa, y velocidad del viento. Los datos meteorológicos utilizados en este sistema, corresponden a salidas del modelo climático GFS (Global Forecast System) que es corrido cada 6 horas por el Centro Nacional para la Predicción del Ambiente, dependiente del servicio meteorológico de los estados unidos. 

En total se descargan por día 84 imágenes, para construir la información de entrada del FWI, para las 3 predicciones (00, 24, 48). 

La información del GFS se descarga a través  del software wget, para adquirir esta información no es necesario contar con ningún tipo de usuario y contraseña. 

Para la descarga de información y la construcción de los índices se ejecuta una rutina de bash (downloader.sh), la cual va llamando subrutinas de R. 

El directorio raíz (donde se encuentran subrutinas y subdirectorios) puede ubicarse en cualquier directorio sin necesidad de modificaciones en el código, los direccionamientos apuntan a sub carpetas existentes dentro de la carpeta general o raíz, donde se encuentran las subrutinas de procesamiento. 

Es muy importante no cambiar los nombres ni borrar ninguno de los directorios que se encuentran en el directorio raíz. 
La estructura del directorio raíz es la siguiente:

Raíz/
 |-BUI/
 |-DMC/
 |-DC/
 |-FFMC/
 |-FWI/
 |-ISI/
 |-gfs/
 |-ncep/
 |-temporales/
 |-paises_proy/
 |-utc_bands/
 |-downloader.sh
 |-converter.R
 |-mosaiquer.R
 |-fwi.R
 |-ini.txt
 |-iniP.txt 
 |-iniPY.txt
 |-watchdog.txt

######
Directorios: 

BUI  -> Directorio que aloja los resultados del índice BUI
DMC  -> Directorio que aloja los resultados del índice DMC
DC   -> Directorio que aloja los resultados del índice DC
FFMC -> Directorio que aloja los resultados del índice FFMC
FWI  -> Directorio que aloja los resultados del índice FWI
ISI  -> Directorio que aloja los resultados del índice ISI

Los anteriores acumulan 3 imágenes por día, a 0 24 y 48 hs de pronóstico. Las que corresponden al día actual deben ser expuestas al público, las anteriores deben ser preservadas, por lo menos las que correspondan a la predicción 0 (del día).

gfs 		-> Directorio que contiene la información climática utilizada para la carga de datos al FWI (se puede publicar o no, y se debe conservar por lo menos una ventana movil de una semana de datos). 

ncep 		-> Directorio donde se alojan los archivos  que se van descargando del sistema NCEP, este se limpia en cada corrida.
temporales 	-> Directorio donde se alojan archivos temporales de pasos intermedios, este se limpia en cada corrida.
paises_proy 	-> Directorio con información de base
utc_bands 	-> Directorio con información de base

Rutinas: 

downloader.sh -> Rutina que descarga la información y corre subprocesos para generación de la información del FWI

converter.R -> Rutina encargada de convertir la información descargada a formato accesible por el sistema y reproyectar a E.P.S.G.4326,los resultados de esta rutina se alojan en temporales. 

mosaiquer.R -> Rutina encargada de generar los mosaicos con la información climática, los resultados de esta rutina se alojan en gfs.

fwi.R -> Rutina que procesa la información climática para obtener los productos del FWI, los resultados de esta rutina se alojan en las carpetas BUI,DMC,DC,FFMC,ISI, y FWI.

ini.txt | iniP.txt iniPY.txt | -> archivos de apoyo para la descarga de información meteorológica. 

watchdog -> archivo temporal utilizado como control de funcionamiento del sistema.


Las subrutinas del software R dependen de los siguiente paquetes o librerías de R.

raster -> paquete para manejar datos espaciales.
cffdrs -> paquete utilizado para calcular los índices de fuego.

El archivo downloader.sh actualiza la información de forma diaria, pero debe ser corrido cada 6 horas (05:00, 11:00, 17:00 y 23:00) para garantizar que los productos se generen. Una vez generados en el día, el sistema solo revisará el watchdog. 








