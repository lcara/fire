#!/bin/bash


SCRIPT=`realpath $0`
dir=`dirname $SCRIPT`

## en superficie
v_su="&lev_surface=on"
## a 2 m del suelo
v_2m="&lev_2_m_above_ground=on"

# línea de descarga general
var0="&file=gfs.t00z.pgrb2.0p25."
var1="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25_1hr.pl?dir=%2Fgfs."
v_tmp="&var_TMP=on" ## temperatura
v_rh="&var_RH=on"   ## Humedad Relativa
v_ws="&var_GUST=on" ## velocidad del viento!
v_pp="&var_APCP=on" ## Precipitación!

## Extent del área de estudio
var4="&subregion=&leftlon=240&rightlon=315&toplat=35&bottomlat=-60"

### fecha del día en curso
var_FECHA_HOY=`date +%Y%m%d -d "today"`
### hoy -1 (ayer)
var_FECHA_AYER=`date +%Y%m%d -d "1 day ago"`

### entro al directorio en cuestión! 
cd $dir/ncep

########################################################################################
########################################################################################
########################################################################################
## si no existe el archivo watchdog es porque el proceso no se terminó en el día!

if [ -e .././watchdog.txt ] ## si el archivo existe! 

	### debo preguntar si coincide con la fecha!
then
echo "existe el wd"
hoy=`date +%Y-%m-%d -d "today"`
d1=`cat .././watchdog.txt`

  if [ "$d1" == "$hoy" ] ### si existe y coincide con la fecha de hoy, eso quiere decir que tiene que haberse corrido hoy!!!
  then 
   echo "PROCESO YA CORRIDO PARA ESTE DÍA"
   exit ### este debería romper todo!!
  else
   echo "el archivo no coincide con la fecha actual por lo que sigo adelante!"
	echo $d1
	echo $hoy
   rm -f .././watchdog.txt
  fi
else
echo " no existe el wd, sigo adelante"
fi

rm *.*
rm .././temporales/*.*
# var1="http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25_1hr.pl?file=gfs.t00z.pgrb2.0p25."

# var3="&lev_2_m_above_g1round=on&var_TMP=on&subregion=&leftlon=240&rightlon=315&toplat=35&bottomlat=-60&dir=%2Fgfs."

# var4=`date +%Y%m%d -d "today"`
 
for j in `cat .././ini.txt`
do

### temperatura!
#wget $var1 $j -=$v_2m $v_tmp=-  $var4 $var5 "00"

wget $var1$var_FECHA_HOY"%2F00"$var0$j$v_2m$v_tmp$var4

Rscript .././converter.R
d1=`ls`
rm -f $d1

### Humedad Relativa 
#wget $var1  $j -=$v_2m $v_rh=- $var4 $var5 "00"

wget $var1$var_FECHA_HOY"%2F00"$var0$j$v_2m$v_rh$var4
     

Rscript .././converter.R
d1=`ls`
rm $d1

## Velocidad del viento!
#wget $var1 $j -=$v_su $v_ws=-  $var4 $var5 "00"

wget $var1$var_FECHA_HOY"%2F00"$var0$j$v_su$v_ws$var4

Rscript .././converter.R
d1=`ls`
rm $d1

done

#### hasta acá todo lo que debe generar para temperatura humedad y velocidad del viento!

#Tengo que armar el acumulado de lluvia!
### comienzo con lluvia!

for j in `cat .././iniPY.txt`
do
echo ""
echo ""
echo ""
echo  $j
echo ""
echo ""
echo ""
# wget $var1 $j -=$v_su $v_pp=- $var4 $var6 "00"

wget $var1$var_FECHA_AYER"%2F00"$var0$j$v_su$v_pp$var4


Rscript .././converter.R
#d1=`ls`
#rm $d1

clear
done	

 # ## Fin de comentario multilínea #


for j in `cat .././iniP.txt`
do

# wget $var1 $j -=$v_su $v_pp=-  $var4 $var5 "00"

wget $var1$var_FECHA_HOY"%2F00"$var0$j$v_su$v_pp$var4

Rscript .././converter.R
d1=`ls`
rm $d1

done	

### tengo que armar los tres mosaicos!

Rscript .././mosaiquer.R


rm .././temporales/*.*

Rscript .././fwi.R




