\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}{\uppercase {T}\IeC {\'e}rminos de referencia}}{2}{section.1}
\contentsline {section}{\numberline {2}{\uppercase {I}ntroducci\IeC {\'o}n}}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}{\IeC {\'A}reas quemadas}}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}{Focos de calor}}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}{\IeC {\'I}ndice de peligro meteorol\IeC {\'o}gico de incendios}}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}{ \uppercase {\IeC {\'a}}rea de estudio}}{5}{section.3}
\contentsline {section}{\numberline {4}{\uppercase {\IeC {\'a}}reas quemadas}}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}{\uppercase {p}roductos calculados}}{5}{subsection.4.1}
\contentsline {section}{\numberline {5}{\uppercase {F}ocos de calor}}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}{\uppercase {p}roductos calculados}}{7}{subsection.5.1}
\contentsline {section}{\numberline {6}{\uppercase {s}istema meterorol\IeC {\'o}gico de peligro de incencios \uppercase {fwi}}}{8}{section.6}
\contentsline {subsection}{\numberline {6.1}{C\IeC {\'o}digo de humedad del combustible fino FFMC}}{8}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}{C\IeC {\'o}digo de humedad del mantillo DMC}}{9}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}{C\IeC {\'o}digo de sequ\IeC {\'\i }a DC}}{9}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}{\IeC {\'I}ndice de propagaci\IeC {\'o}n inicial ISI}}{10}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}{\IeC {\'I}ndice de combustible disponible BUI}}{10}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}{FWI}}{10}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}{ Informaci\IeC {\'o}n meteorol\IeC {\'o}gica}}{10}{subsection.6.7}
\contentsline {section}{\numberline {7}{\uppercase {conclusiones}}}{11}{section.7}
