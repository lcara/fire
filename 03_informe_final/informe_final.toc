\select@language {spanish}
\contentsline {section}{\numberline {1}\uppercase {t}\IeC {\'e}rminos de referencia}{2}{section.1}
\contentsline {section}{\numberline {2}\uppercase {I}ntroducci\IeC {\'o}n}{2}{section.2}
\contentsline {section}{\numberline {3} \uppercase {\IeC {\'a}}rea de estudio}{3}{section.3}
\contentsline {section}{\numberline {4}\uppercase {\IeC {\'a}}reas quemadas}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}\uppercase {I}ntroducci\IeC {\'o}n}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}\uppercase {C}obertura del \IeC {\'a}rea de estudio}{4}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}\uppercase {p}roductos elaborados}{4}{subsection.4.3}
\contentsline {section}{\numberline {5}\uppercase {F}ocos de calor}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}\uppercase {i}ntroducci\IeC {\'o}n}{6}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\uppercase {C}obertura del \IeC {\'a}rea de estudio}{8}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}\uppercase {p}roductos elaborados}{8}{subsection.5.3}
\contentsline {section}{\numberline {6}\uppercase {s}istema meteorol\IeC {\'o}gico de peligro de incendios \uppercase {(fwi)}}{9}{section.6}
\contentsline {subsection}{\numberline {6.1}\uppercase {\IeC {\'I}}ntroducci\IeC {\'o}n}{9}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}\uppercase {C}obertura del \IeC {\'a}rea de estudio}{10}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3} Informaci\IeC {\'o}n meteorol\IeC {\'o}gica}{10}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}\uppercase {P}roductos elaborados}{11}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}C\IeC {\'o}digo de humedad del combustible fino FFMC}{11}{subsubsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.2}C\IeC {\'o}digo de humedad del mantillo DMC}{11}{subsubsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.3}C\IeC {\'o}digo de sequ\IeC {\'\i }a DC}{11}{subsubsection.6.4.3}
\contentsline {subsubsection}{\numberline {6.4.4}\IeC {\'I}ndice de propagaci\IeC {\'o}n inicial ISI}{12}{subsubsection.6.4.4}
\contentsline {subsubsection}{\numberline {6.4.5}\IeC {\'I}ndice de combustible disponible BUI}{12}{subsubsection.6.4.5}
\contentsline {subsubsection}{\numberline {6.4.6}FWI}{12}{subsubsection.6.4.6}
\contentsline {section}{\numberline {7}\uppercase {c}onclusiones}{13}{section.7}
